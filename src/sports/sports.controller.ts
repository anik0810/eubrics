import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { SportsService } from './sports.service';
import { CreateSportDto } from './dto/create-sport.dto';
import { UpdateSportDto } from './dto/update-sport.dto';
import { ApiTags } from '@nestjs/swagger';


@ApiTags('Sports controller')
@Controller('sports')
export class SportsController {
  constructor(private readonly sportsService: SportsService) {}

  @Post('addSport')
  create(@Body() createSportDto: CreateSportDto) {
    return this.sportsService.create(createSportDto);
  }

  @Get()
  findAll() {
    return this.sportsService.findAll();
  }

  @Get('getSport/:userId')
  findOne(@Param('userId') userId: string) {
    return this.sportsService.findSportUser(userId);
  }

  @Patch('updateSport/:id')
  update(@Param('id') id: string, @Body() updateSportsDto: UpdateSportDto) {
    return this.sportsService.update(+id, updateSportsDto);
  }

  @Delete('deleteSport/:id/:userId')
  remove(@Param('id') id: string,@Param('userId') userId: string) {
    return this.sportsService.remove(+id,userId);
  }
}
