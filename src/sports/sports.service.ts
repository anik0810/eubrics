import { Injectable } from '@nestjs/common';
import { CreateSportDto } from './dto/create-sport.dto';
import { UpdateSportDto } from './dto/update-sport.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Sport } from './entities/sport.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SportsService {

  constructor(
    @InjectRepository(Sport)
    private sportRepository: Repository<Sport>,
  ) { }

  create(createSportsDto: CreateSportDto) {

    const sport=new Sport();

    sport.userId=createSportsDto.userId;
    sport.todo=createSportsDto.todo;
    sport.lastUpdate=createSportsDto.lastUpdate;
    sport.id=100;

    return this.sportRepository.save(sport);
  }

  findAll() {
    return this.sportRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} Study`;
  }

  findSportUser(userId: string) {
    return this.sportRepository.find({
      where:{
        userId:userId
      }
    });
  }

  async update(id: number, updateSportDto: UpdateSportDto) {
    const sport=new Sport();
    sport.id=id;
    sport.completed=updateSportDto.completed;
    sport.lastUpdate=updateSportDto.lastUpdate;
    sport.todo=updateSportDto.todo;
    sport.userId=updateSportDto.userId;
    await this.sportRepository.save(sport);
    return this.findSportUser(sport.userId);
  }

  async remove(id: number,userId:string) {
    await this.sportRepository.delete(id);
    return this.findSportUser(userId);
  }
}
