import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { StudyService } from './study.service';
import { CreateStudyDto } from './dto/create-study.dto';
import { UpdateStudyDto } from './dto/update-study.dto';
import { ApiTags } from '@nestjs/swagger';


@ApiTags('Study controller')
@Controller('study')
export class StudyController {
  constructor(private readonly studyService: StudyService) {}

  @Post('addStudy')
  create(@Body() createStudyDto: CreateStudyDto) {
    return this.studyService.create(createStudyDto);
  }

  @Get()
  findAll() {
    return this.studyService.findAll();
  }

  @Get('getStudy/:userId')
  findOne(@Param('userId') userId: string) {
    return this.studyService.findStudyUser(userId);
  }

  @Patch('updateStudy/:id')
  update(@Param('id') id: string, @Body() updateStudyDto: UpdateStudyDto) {
    return this.studyService.update(+id, updateStudyDto);
  }

  @Delete('deleteHealth/:id/:userId')
  remove(@Param('id') id: string,@Param('userId') userId: string) {
    return this.studyService.remove(+id,userId);
  }
  
}