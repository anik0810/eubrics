import { Injectable } from '@nestjs/common';
import { CreateStudyDto } from './dto/create-study.dto';
import { UpdateStudyDto } from './dto/update-study.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Study } from './entities/study.entity';
import { Repository } from 'typeorm';

@Injectable()
export class StudyService {

  constructor(
    @InjectRepository(Study)
    private studyRepository: Repository<Study>,
  ) { }

  create(createStudyDto: CreateStudyDto) {

    const study=new Study();

    study.userId=createStudyDto.userId;
    study.todo=createStudyDto.todo;
    study.lastUpdate=createStudyDto.lastUpdate;
    study.id=100;

    return this.studyRepository.save(study);
  }

  findAll() {
    return this.studyRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} Study`;
  }

  findStudyUser(userId: string) {
    return this.studyRepository.find({
      where:{
        userId:userId
      }
    });
  }

  async update(id: number, updateStudyDto: UpdateStudyDto) {
    const study=new Study();
    study.id=id;
    study.completed=updateStudyDto.completed;
    study.lastUpdate=updateStudyDto.lastUpdate;
    study.todo=updateStudyDto.todo;
    study.userId=updateStudyDto.userId;
    await this.studyRepository.save(study);
    return this.findStudyUser(study.userId);
  }

  async remove(id: number,userId:string) {
    await this.studyRepository.delete(id);
    return this.findStudyUser(userId);
  }
}
