import { Controller, Get, Post, Body, Patch, Param, Delete,HttpCode } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateAuthDto, UserCredentials } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { ApiTags } from '@nestjs/swagger';


@ApiTags('Auth controller')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('signUp')
  @HttpCode(200)
  create(@Body() createAuthDto: CreateAuthDto) {
    return this.authService.create(createAuthDto);
  }

  @Post('signIn')
  @HttpCode(200)
  logIn(@Body() userCredential:UserCredentials ) {
    return this.authService.login(userCredential.email,userCredential.password);
  }

}
