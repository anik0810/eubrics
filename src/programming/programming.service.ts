import { Injectable } from '@nestjs/common';
import { CreateProgrammingDto } from './dto/create-programming.dto';
import { UpdateProgrammingDto } from './dto/update-programming.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Programming } from './entities/programming.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProgrammingService {

  constructor(
    @InjectRepository(Programming)
    private programmingRepository: Repository<Programming>,
  ) { }

  create(createProgrammingDto: CreateProgrammingDto) {
    const programming=new Programming();

    programming.userId=createProgrammingDto.userId;
    programming.todo=createProgrammingDto.todo;
    programming.lastUpdate=createProgrammingDto.lastUpdate;
    programming.id=100;

    return this.programmingRepository.save(programming);
  }

  findAll() {
    return this.programmingRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} Programming`;
  }

  findProgrammingUser(userId: string) {
    return this.programmingRepository.find({
      where:{
        userId:userId
      }
    });
  }

  update(id: number, updateProgrammingDto: UpdateProgrammingDto) {
    const programming=new Programming();
    programming.id=id;
    programming.completed=updateProgrammingDto.completed;
    programming.lastUpdate=updateProgrammingDto.lastUpdate;
    programming.todo=updateProgrammingDto.todo;
    programming.userId=updateProgrammingDto.userId;
    return this.programmingRepository.save(programming);
  }

  remove(id: number) {
    return this.programmingRepository.delete(id);
  }
}
